//
//  PTRCompanyDetailViewController.h
//  PetronautApp
//
//  Created by Luis Fernando Mata on 13/10/15.
//  Copyright © 2015 Petronaut. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PTRCompanyDetailViewController : UIViewController

@property (nonatomic, strong) NSDictionary *companyDictionary;

@end
