//
//  PTRCompanyDetailViewController.m
//  PetronautApp
//
//  Created by Luis Fernando Mata on 13/10/15.
//  Copyright © 2015 Petronaut. All rights reserved.
//

#import "PTRCompanyDetailViewController.h"
#import <CSStickyHeaderFlowLayout/CSStickyHeaderFlowLayout.h>
#import "MKMapView+PetronautMap.h"
#import "PTRFullMapViewController.h"
#import "PTRHeaderTableViewCell.h"
#import "PTRDetailTableViewCell.h"
#import "PTRSearchCompaniesViewController.h"
#import "PTRHeaderView.h"
#import <MXParallaxHeader/MXParallaxHeader.h>
#import <Social/Social.h>
#import <Google/Analytics.h>

@interface PTRCompanyDetailViewController () <UITableViewDelegate, UITableViewDataSource, PTRDetailTableViewCellDelegate>{
    NSArray *informationArray;
    NSString *phoneNumber;
    NSString *selectedTag;
    NSString *selectedHeader;
    __weak IBOutlet UITableView *detailTableView;
}

@property (nonatomic, strong) UINib *headerNib;

@end

@implementation PTRCompanyDetailViewController

@synthesize companyDictionary;

static NSString * const reuseIdentifier = @"DetailCell";
static NSString * const reuseIdentifierHeader = @"HeaderCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Parallax Header
    PTRHeaderView *headerView = [PTRHeaderView instanciateFromNib];
    [headerView.mapView setPlaceMarkForCompany:companyDictionary];
    [headerView.seeFullMapButton addTarget:self action:@selector(seeFullMap) forControlEvents:UIControlEventTouchUpInside];
    detailTableView.parallaxHeader.view = headerView;
    detailTableView.parallaxHeader.height = (self.view.frame.size.height / 3.0);
    detailTableView.parallaxHeader.mode = MXParallaxHeaderModeFill;
    detailTableView.parallaxHeader.minimumHeight = 45;
    
    detailTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    //Set array information to dynamically create tables
    informationArray = [self createArrayOfInformation];
    
    //navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Action, target: self, action: "shareTapped")
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(shareCompany)];
    
    self.title = [companyDictionary objectForKey:@"company"];
    
    // Also insets the scroll indicator so it appears below the search bar
    
    NSString *companyID = [self.companyDictionary objectForKey:@"id"];
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Company detail"
                                                          action:@"See"
                                                           label:companyID
                                                           value:@1] build]];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Company detail"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [[GAI sharedInstance] dispatch];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Manage data

- (NSArray *) createArrayOfInformation{
    NSMutableArray *infoArray = [[NSMutableArray alloc] init];
    
    NSDictionary *dataDictionary = @{@"header" : @"Information", @"data" : @""};
    [infoArray addObject:dataDictionary];
    
    NSString *textString = [companyDictionary objectForKey:@"company"];
    if (![textString isEqualToString:@""]) {
        NSString *companyName = [NSString stringWithString:textString];
        NSDictionary *dataDictionary = @{@"header" : @"Name", @"data" : companyName};
        [infoArray addObject:dataDictionary];
    }
    
    textString = [companyDictionary objectForKey:@"address"];
    if (![textString isEqualToString:@""]) {
        NSString *address = [NSString stringWithString:textString];
        NSDictionary *dataDictionary = @{@"header" : @"Address", @"data" : address};
        [infoArray addObject:dataDictionary];
    }
    
    textString = [NSString stringWithFormat:@"%@, %@", [companyDictionary objectForKey:@"city" ], [companyDictionary objectForKey:@"state"]];
    if (![textString isEqualToString:@", "]) {
        NSString *cityInfo = [NSString stringWithString:textString];
        NSDictionary *dataDictionary = @{@"header" : @"City", @"data" : cityInfo};
        [infoArray addObject:dataDictionary];
    }
    
    textString = [companyDictionary objectForKey:@"zipcode"];
    if (![textString isEqualToString:@""]) {
        NSString *zipcode = [NSString stringWithString:textString];
        NSDictionary *dataDictionary = @{@"header" : @"Zipcode", @"data" : zipcode};
        [infoArray addObject:dataDictionary];
    }
    
    textString = [NSString stringWithFormat:@"%@", [companyDictionary objectForKey:@"office" ]];
    if (![textString isEqualToString:@" "]) {
        NSString *number = [NSString stringWithString:textString];
        phoneNumber = [number stringByReplacingOccurrencesOfString:@"-" withString:@""];
        phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *secondNumber = [companyDictionary objectForKey:@"dispatch"];
        if (secondNumber) {
            number = [NSString stringWithFormat:@"%@, %@", number, secondNumber];
        }
        
        if (phoneNumber.length > 12) {
            phoneNumber = [phoneNumber substringWithRange:NSMakeRange(0, 12)];
        }
        NSDictionary *dataDictionary = @{@"header" : @"Phone", @"data" : number};
        [infoArray addObject:dataDictionary];
    }
    
    textString = [companyDictionary objectForKey:@"website"];
    
    dataDictionary = @{@"header" : @"Detail", @"data" : @""};
    [infoArray addObject:dataDictionary];
    
    textString = [companyDictionary objectForKey:@"products"];
    if (![textString isEqualToString:@""] && textString) {
        NSString *products = [NSString stringWithString:textString];
        NSDictionary *dataDictionary = @{@"header" : @"Products", @"data" : products};
        [infoArray addObject:dataDictionary];
    }
    
    textString = [companyDictionary objectForKey:@"services"];
    if (![textString isEqualToString:@""] && textString) {
        NSString *services = [NSString stringWithString:textString];
        NSDictionary *dataDictionary = @{@"header" : @"Services", @"data" : services};
        [infoArray addObject:dataDictionary];
    }
    
    textString = [companyDictionary objectForKey:@"sector"];
    if (![textString isEqualToString:@""] && textString) {
        NSString *sector = [NSString stringWithString:textString];
        NSDictionary *dataDictionary = @{@"header" : @"Sector", @"data" : sector};
        [infoArray addObject:dataDictionary];
    }
    
    return infoArray;
}

#pragma mark UICollectionViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return informationArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *data = [informationArray objectAtIndex:indexPath.row];
    NSString *header = [data objectForKey:@"header"];
    
    if (([header isEqualToString:@"Information"]) || ([header isEqualToString:@"Detail"])) {
        PTRHeaderTableViewCell *cell = (PTRHeaderTableViewCell *) [tableView dequeueReusableCellWithIdentifier:reuseIdentifierHeader];
        cell.userInteractionEnabled = NO;
        cell.headerLabel.text = header;
        return cell;
    } else {
        PTRDetailTableViewCell *cell = (PTRDetailTableViewCell *) [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if ([header isEqualToString:@"Services"] || [header isEqualToString:@"Products"]) {
            cell.delegate = self;
            [cell renderWithTagsForDictionary:data];
        } else {
            [cell renderCellWithDictionary:data];
            [cell layoutSubviews];
        }
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

#pragma mark UICollectionViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PTRDetailTableViewCell *cell = (PTRDetailTableViewCell *) [tableView cellForRowAtIndexPath:indexPath];
    
    if ([[cell cellHeader] isEqualToString:@"Phone"]) {
        id tracker = [[GAI sharedInstance] defaultTracker];
        NSString *companyID = [self.companyDictionary objectForKey:@"id"];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Action"
                                                              action:@"Call company"
                                                               label:companyID
                                                               value:@1] build]];
        
        NSString *companyPhone = [@"tel://" stringByAppendingString:phoneNumber];
        NSURL *phoneURL = [NSURL URLWithString:companyPhone];
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
}

#pragma mark - Actions

- (void) seeFullMap{
    [self performSegueWithIdentifier:@"showFullMap" sender:self];
}

- (void) shareCompany {
    NSString *textToShare = [NSString stringWithFormat:@"Check %@'s page on Petronaut.", [companyDictionary objectForKey:@"company"]];
    NSString *companyURL = [NSString stringWithFormat:@"https://www.gopetronaut.com/petronaut/more/%@", [companyDictionary objectForKey:@"id"]];
    NSURL *companyWebsite = [NSURL URLWithString:companyURL];
    NSArray *objectsToShare = @[textToShare, companyWebsite];
    UIActivityViewController *vc = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo,
                                   UIActivityTypeOpenInIBooks,
                                   UIActivityTypePostToTencentWeibo];
    vc.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:vc animated:true completion:nil];
}

- (void)didSelectTagWithString:(NSString *)selectedString forHeader:(NSString *)header {
    selectedHeader = header;
    selectedTag = selectedString;
    [self performSegueWithIdentifier:@"SelectedTag" sender:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"showFullMap"]) {
        PTRFullMapViewController *nextViewController = (PTRFullMapViewController *) [segue destinationViewController];
        nextViewController.companyDictionary = companyDictionary;
    }
    
    if ([segue.identifier isEqualToString:@"SelectedTag"]) {
        PTRSearchCompaniesViewController *nextVC = (PTRSearchCompaniesViewController *) [segue destinationViewController];
        nextVC.searchService = selectedTag;
        nextVC.isService = [selectedHeader isEqualToString:@"Services"];
    }
}

@end
