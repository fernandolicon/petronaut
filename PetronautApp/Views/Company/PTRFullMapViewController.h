//
//  PTRFullMapViewController.h
//  PetronautApp
//
//  Created by Luis Fernando Mata on 17/10/15.
//  Copyright © 2015 Petronaut. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MKMapView;

@interface PTRFullMapViewController : UIViewController

@property (weak, nonatomic) IBOutlet MKMapView *fullMap;
@property (strong, nonatomic) NSDictionary *companyDictionary;

@end
