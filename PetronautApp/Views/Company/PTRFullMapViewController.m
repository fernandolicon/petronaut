//
//  PTRFullMapViewController.m
//  PetronautApp
//
//  Created by Luis Fernando Mata on 17/10/15.
//  Copyright © 2015 Petronaut. All rights reserved.
//

#import "PTRFullMapViewController.h"
#import "MKMapView+PetronautMap.h"
#import "PTRLocationHelper.h"
#import "JGProgressHUD.h"
#import "PTRUtils.h"
#import <Google/Analytics.h>

@interface PTRFullMapViewController () <MKMapViewDelegate> {
    JGProgressHUD *progressHUD;
}

@end

@implementation PTRFullMapViewController

MKRoute *routeDetails;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.fullMap.showsUserLocation = YES;
    [self.fullMap setPlaceMarkForCompany:self.companyDictionary];
    if ([[PTRLocationHelper sharedManager] hasAuthorizationFromUser]) {
        UIBarButtonItem *gpsButton  = [[UIBarButtonItem alloc] initWithImage: [UIImage imageNamed:@"gpsIcon"] landscapeImagePhone:nil style:UIBarButtonItemStylePlain target:self action:@selector(showDirections)];
        self.navigationItem.rightBarButtonItem = gpsButton;
    }
    
    progressHUD = [[JGProgressHUD alloc] initWithStyle:JGProgressHUDStyleDark];
    progressHUD.textLabel.text = @"Calculating";
    
    self.fullMap.delegate = self;
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Full map"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) showDirections {
    [progressHUD showInView:self.view];
    id tracker = [[GAI sharedInstance] defaultTracker];
    NSString *companyID = [self.companyDictionary objectForKey:@"id"];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Action"
                                                          action:@"GPS"
                                                           label:companyID
                                                           value:@1] build]];
    
    MKDirectionsRequest *directionsRequest = [[MKDirectionsRequest alloc] init];
    [PTRUtils placeMarkForCompany:self.companyDictionary withSuccess:^(MKPlacemark *placemark) {
        [directionsRequest setSource:[MKMapItem mapItemForCurrentLocation]];
        [directionsRequest setDestination:[[MKMapItem alloc] initWithPlacemark:placemark]];
        directionsRequest.transportType = MKDirectionsTransportTypeAutomobile;
        MKDirections *directions = [[MKDirections alloc] initWithRequest:directionsRequest];
        [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
            [progressHUD dismiss];
            if (error) {
                NSLog(@"Error %@", error.description);
            } else {
                routeDetails = response.routes.lastObject;
                [self.fullMap addOverlay:routeDetails.polyline];
                [self.fullMap setVisibleMapRect:routeDetails.polyline.boundingMapRect edgePadding:UIEdgeInsetsMake(20.0, 20.0, 20.0, 20.0) animated:YES];
            }
        }];
    }];
}

-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    MKPolylineRenderer  * routeLineRenderer = [[MKPolylineRenderer alloc] initWithPolyline:routeDetails.polyline];
    routeLineRenderer.strokeColor = [UIColor colorWithRed:42/255.f green:107/255.f blue:196/255.f alpha:0.9];
    routeLineRenderer.lineWidth = 5;
    return routeLineRenderer;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
