//
//  PTRCompaniesViewController.m
//  PetronautApp
//
//  Created by Luis Fernando Mata on 24/8/15.
//  Copyright (c) 2015 Petronaut. All rights reserved.
//

#import "PTRCompaniesViewController.h"
#import <JGProgressHUD/JGProgressHUD.h>
#import <TMCache/TMCache.h>
#import "PTRCompanyDetailViewController.h"
#import "PTRRequestHelper.h"
#import "PTRViewHelper.h"
#import "PTRCompaniesTableViewCell.h"
#import <Google/Analytics.h>

@interface PTRCompaniesViewController()<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>{
    NSDictionary *selectedCompany;
    NSDictionary *companies;
    NSArray *companiesSectionTitles;
    __weak IBOutlet UITableView *companiesTableView;
    JGProgressHUD *progressHUD;
    __weak IBOutlet UISearchBar *companiesSearchBar;
}

@end

@implementation PTRCompaniesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    companiesTableView.tableFooterView = [UIView new];
    
    progressHUD = [[JGProgressHUD alloc] initWithStyle:JGProgressHUDStyleDark];
    progressHUD.textLabel.text = @"Loading";
    
    [companiesSearchBar setHidden:YES];
    
    self.title = [self getSectorName];
    
    selectedCompany = [[NSDictionary alloc] init];
    
    [self getViewData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    NSString *name = [NSString stringWithFormat:@"List %@ companies", [self getSectorName]];
    [tracker set:kGAIScreenName value:name];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void) getViewData{
    NSString *sectorString = [self getSectorName];
    NSDictionary *cachedSector = [[TMCache sharedCache] objectForKey:sectorString];
    
    if (!cachedSector){
        [self performConnection];
    }else{
        companies = [cachedSector objectForKey:@"companies"];
        companiesSectionTitles = [cachedSector objectForKey:@"sections"];
    }
}

- (NSString *) getSectorName{
    switch (self.selectedSector) {
        case 0:
            return @"E&P";
            break;
        case 1:
            return @"Drilling";
            break;
        case 2:
            return @"Well Services";
            break;
        case 3:
            return @"Environment / Safety / IT";
            break;
        case 4:
            return @"Rentals / Sales";
            break;
        case 5:
            return @"Manufacturing / Refining";
        case 6:
            return @"Tubulars";
            break;
        case 7:
            return @"Trucking";
            break;
        default:
            return @"";
            break;
    }
}

#pragma mark - Data

- (void) populateDataWithResponse: (NSDictionary *) response{
    companies = [PTRViewHelper companiesAlphabeticallyFromResponse:response];
    companiesSectionTitles = [[companies allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [self showConnectionPerformedSuccessful:YES];
}

- (NSDictionary *) companyForIndexPath: (NSIndexPath *) indexPath{
    NSString *sectionTitle = [companiesSectionTitles objectAtIndex:indexPath.section];
    NSArray *sectionCompanies = [companies objectForKey:sectionTitle];
    return [sectionCompanies objectAtIndex:indexPath.row];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return [companiesSectionTitles count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [companiesSectionTitles objectAtIndex:section];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return companiesSectionTitles;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSString *sectionTitle = [companiesSectionTitles objectAtIndex:section];
    NSArray *sectionCompanies = [companies objectForKey:sectionTitle];
    return [sectionCompanies count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PTRCompaniesTableViewCell *cell = (PTRCompaniesTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"companyCell" forIndexPath:indexPath];
    
    NSDictionary *provisionalCompany = [self companyForIndexPath:indexPath];
    [cell renderWithCompanyDictionary:provisionalCompany];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    selectedCompany = [self companyForIndexPath:indexPath];
    [self performSegueWithIdentifier:@"showCompanyDetail" sender:nil];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

#pragma mark - Network methods

- (void) performConnection{
    [progressHUD showInView:self.view animated:YES];
    [PTRRequestHelper getCompaniesForSector:self.selectedSector withSuccess:^(NSDictionary *response) {
        [self populateDataWithResponse:response];
    } withFailure:^(NSError *error) {
        [self requestErrorInConnection];
    }];
}

- (void) requestErrorInConnection{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error." message:@"There was an error with the server. Please try again later." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    [self showConnectionPerformedSuccessful:NO];
}

- (void) showConnectionPerformedSuccessful: (BOOL) wasSuccessful{
    [progressHUD dismissAnimated:YES];
    
    if (wasSuccessful) {
        NSString *sectorString = [self getSectorName];
        NSDictionary *companiesFullList = @{@"sections" : companiesSectionTitles, @"companies" : companies};
        [[TMCache sharedCache] setObject:companiesFullList forKey:sectorString block:nil];
        [companiesTableView reloadData];
        [companiesSearchBar setHidden:NO];
        [companiesTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showCompanyDetail"]) {
        PTRCompanyDetailViewController *nextVC = [segue destinationViewController];
        nextVC.companyDictionary = selectedCompany;
    }
}

@end
