//
//  PTRSearchCompaniesViewController.h
//  PetronautApp
//
//  Created by Luis Fernando Mata on 4/11/15.
//  Copyright © 2015 Petronaut. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PTRSearchCompaniesViewController : UIViewController

@property (nonatomic, strong) NSString *searchService;
@property BOOL isService;

@end
