//
//  PTRCompaniesViewController.h
//  PetronautApp
//
//  Created by Luis Fernando Mata on 24/8/15.
//  Copyright (c) 2015 Petronaut. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JGProgressHUD;

@interface PTRCompaniesViewController : UIViewController

@property (nonatomic, strong) NSArray *companiesFetched;
@property NSInteger selectedSector;

@end
