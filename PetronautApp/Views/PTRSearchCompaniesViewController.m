//
//  PTRSearchCompaniesViewController.m
//  PetronautApp
//
//  Created by Luis Fernando Mata on 4/11/15.
//  Copyright © 2015 Petronaut. All rights reserved.
//

#import "PTRSearchCompaniesViewController.h"
#import <JGProgressHUD/JGProgressHUD.h>
#import <TMCache/TMCache.h>
#import "PTRCompanyDetailViewController.h"
#import "PTRViewHelper.h"
#import "PTRCompaniesTableViewCell.h"
#import "PTRUtils.h"
#import <Google/Analytics.h>

@interface PTRSearchCompaniesViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>{
    __weak IBOutlet UISearchBar *companiesSearchBar;
    __weak IBOutlet UISegmentedControl *filterControl;
    __weak IBOutlet UITableView *companiesTableView;
    NSDictionary *companies;
    NSArray *companiesSectionTitles;
    NSArray *companiesUnordered;
    NSDictionary *selectedCompany;
    JGProgressHUD *progressHUD;
}

@end

@implementation PTRSearchCompaniesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    progressHUD = [[JGProgressHUD alloc] initWithStyle:JGProgressHUDStyleDark];
    progressHUD.textLabel.text = @"Loading";
    
    companiesUnordered = [[NSArray alloc] init];
    
    if (self.searchService) {
        companiesSearchBar.text = self.searchService;
        NSInteger selectedIndex = self.isService ? 3 : 2;
        [filterControl setSelectedSegmentIndex:selectedIndex];
        [self searchCompaniesWithAutoComplete:NO forSearchText:self.searchService];
    }
    
    [self showLastSearch];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Search"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Data

- (void) populateDataWithResponse: (NSDictionary *) response withAutoComplete: (BOOL) autoComplete forSearchText: (NSString *) searchText{
    companiesSectionTitles = nil;
    companiesUnordered = nil;
    
    if (!autoComplete) {
        companies = [PTRViewHelper companiesAlphabeticallyFromResponse:response];
        companiesSectionTitles = [[companies allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    } else {
        PTRFilter searchFilter = filterControl.selectedSegmentIndex;
        companiesUnordered = [PTRViewHelper filterAutoCompleteWithResponse:response andFilter:searchFilter forText:searchText];
    }
}

- (NSDictionary *) companyForIndexPath: (NSIndexPath *) indexPath{
    NSString *sectionTitle = [companiesSectionTitles objectAtIndex:indexPath.section];
    NSArray *sectionCompanies = [companies objectForKey:sectionTitle];
    return [sectionCompanies objectAtIndex:indexPath.row];
}

- (void) showLastSearch{
    NSDictionary *cachedSearch = [[TMCache sharedCache] objectForKey:@"lastSearch"];
    
    if (cachedSearch){
        companies = [cachedSearch objectForKey:@"companies"];
        companiesSectionTitles = [cachedSearch objectForKey:@"sections"];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return companiesSectionTitles ? [companiesSectionTitles count] : 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return companiesSectionTitles ? [companiesSectionTitles objectAtIndex:section] : nil;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return companiesSectionTitles;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (companiesSectionTitles) {
        NSString *sectionTitle = [companiesSectionTitles objectAtIndex:section];
        NSArray *sectionCompanies = [companies objectForKey:sectionTitle];
        return [sectionCompanies count];
    }
    
    return companiesUnordered.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PTRCompaniesTableViewCell *cell = (PTRCompaniesTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"foundCompanyCell" forIndexPath:indexPath];
    
    if (companiesSectionTitles) {
        NSDictionary *provisionalCompany = [self companyForIndexPath:indexPath];
        [cell renderWithCompanyDictionary:provisionalCompany];
    } else {
        PTRFilter searchFilter = filterControl.selectedSegmentIndex;
        
        if (searchFilter == PTRName || searchFilter == PTRAddress) {
            NSDictionary *provisionalCompany = [companiesUnordered objectAtIndex:indexPath.row];
            [cell renderWithCompanyDictionary:provisionalCompany];
        } else {
            NSString *cellString = [companiesUnordered objectAtIndex:indexPath.row];
            [cell renderForAutocomplete:cellString];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    PTRFilter searchFilter = filterControl.selectedSegmentIndex;
    if (companiesSectionTitles || searchFilter == PTRName || searchFilter == PTRAddress) {
        selectedCompany = companiesSectionTitles ? [self companyForIndexPath:indexPath] : [companiesUnordered objectAtIndex:indexPath.row];
        [self performSegueWithIdentifier:@"showFoundCompany" sender:nil];
    } else {
        NSString *searchText = [companiesUnordered objectAtIndex:indexPath.row];
        [self searchCompaniesWithAutoComplete:NO forSearchText:searchText];
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

#pragma mark - Search Bar delegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    NSString *searchText = searchBar.text;
    if (![searchText isEqualToString:@""] && searchText.length >= 3) {
        [searchBar resignFirstResponder];
        [self searchCompaniesWithAutoComplete:NO forSearchText:searchText];
    } else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Too short" message:@"Please enter three or more characters." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (![searchText isEqualToString:@""] && searchText.length >= 3) {
        [self searchCompaniesWithAutoComplete:YES forSearchText:searchText];
    }
}

#pragma mark - Network

- (void) searchCompaniesWithAutoComplete: (BOOL) autoComplete forSearchText: (NSString *) searchText{
    [progressHUD showInView:self.view];
    PTRFilter searchFilter = filterControl.selectedSegmentIndex;
    NSString *searchString = companiesSearchBar.text;
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Action"
                                                          action:@"Search"
                                                           label:[PTRUtils stringForFilter:searchFilter ]
                                                           value:@1] build]];
    
    if (![searchString isEqualToString:@""]) {
        [PTRUtils searchCompaniesWithFilter:searchFilter withString:searchString withAutoComplete:autoComplete withSuccess:^(NSDictionary *response) {
            [self populateDataWithResponse:response withAutoComplete: autoComplete forSearchText:searchText];
            [self responseFromServerSuccessful:YES withAutoComplete:autoComplete];
        } withFailure:^(NSError *error) {
            [self responseFromServerSuccessful:NO withAutoComplete:autoComplete];
        }];
    }else{
        //TODO: Let user know there's no search string
    }
}

- (void) responseFromServerSuccessful: (BOOL) successful withAutoComplete: (BOOL) autoComplete{
    [progressHUD dismiss];
    
    if (successful) {
        [companiesTableView reloadData];
        if (!autoComplete) {
            NSDictionary *companiesFullList = @{@"sections" : companiesSectionTitles, @"companies" : companies};
            [[TMCache sharedCache] setObject:companiesFullList forKey:@"lastSearch" block:nil];
        }
    }else{
        //TODO: Show error
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"showFoundCompany"]) {
        PTRCompanyDetailViewController *nextViewController = (PTRCompanyDetailViewController *) segue.destinationViewController;
        nextViewController.companyDictionary = selectedCompany;
    }
}

@end
