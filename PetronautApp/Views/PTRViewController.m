//
//  ViewController.m
//  PetronautApp
//
//  Created by Luis Fernando Mata on 11/8/15.
//  Copyright (c) 2015 Petronaut. All rights reserved.
//

#import "PTRViewController.h"
#import "PTRSectorsCell.h"
#import "PTRCompaniesViewController.h"
#import <Google/Analytics.h>

@interface PTRViewController ()<UITableViewDataSource, UITableViewDelegate>{
    __weak IBOutlet UITableView *sectorsTableView;
    NSArray *sectorsNames;
    NSArray *images;
    NSInteger selectedSector;
}

@end

@implementation PTRViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    sectorsTableView.tableFooterView = [UIView new];
    
    sectorsNames = @[@"E&P", @"Drilling", @"Well Services", @"Environment / Safety / IT", @"Rentals / Sales", @"Manufacturing / Refining", @"Tubulars", @"Trucking"];
    images = @[@"ep_icon", @"drilling_icon", @"well_services_icon", @"safety_icon", @"rental_icon", @"refining_icon", @"tubular_icon", @"trucking_icon"];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Home"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return sectorsNames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PTRSectorsCell *cell = (PTRSectorsCell *)[tableView dequeueReusableCellWithIdentifier:@"sectorCell" forIndexPath:indexPath];
    
    cell.sectorName.text = [sectorsNames objectAtIndex:indexPath.row];
    UIImage *sectorImage = [UIImage imageNamed:[images objectAtIndex:indexPath.row]];
    [cell.sectorIcon setContentMode:UIViewContentModeScaleAspectFit];
    [cell.sectorIcon setImage:sectorImage];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    selectedSector = indexPath.row;
    [self performSegueWithIdentifier:@"showCompany" sender:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90.0f;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"showCompany"]) {
        PTRCompaniesViewController *nextVC = (PTRCompaniesViewController *) segue.destinationViewController;
        nextVC.selectedSector = selectedSector;
    }
}

@end
