//
//  PTRUtils.m
//  PetronautApp
//
//  Created by Luis Fernando Mata on 10/1/16.
//  Copyright © 2016 Petronaut. All rights reserved.
//

#import "PTRUtils.h"

@implementation PTRUtils

+ (NSString *) stringForFilter: (PTRFilter) filter {
    switch (filter) {
        case PTRName:
            return @"company";
        case PTRServices:
            return @"services";
        case PTRAddress:
            return @"address";
        case PTRProducts:
            return @"products";
        default:
            return @"";
            break;
    }
}

+ (void)searchCompaniesWithFilter: (PTRFilter) searchFilter withString: (NSString *) searchString withAutoComplete: (BOOL) autoComplete withSuccess:(void (^)(NSDictionary *response))success withFailure: (void (^)(NSError *error))failure {
    if (autoComplete) {
        [PTRRequestHelper performAutoCompleteWithFilter:searchFilter withString:searchString withSuccess:^(NSDictionary *response) {
            success(response);
        } withFailure:^(NSError *error) {
            failure(error);
        }];
    } else {
        [PTRRequestHelper searchCompaniesWithFilter:searchFilter withString:searchString withSuccess:^(NSDictionary *response) {
            success(response);
        } withFailure:^(NSError *error) {
            failure(error);
        }];
    }
}

+ (void) placeMarkForCompany: (NSDictionary *) companyDictionary withSuccess:(void (^)(MKPlacemark *placeMark))success {
    NSString *address = [[companyDictionary objectForKey:@"address"] isEqualToString:@"PO Box"] ? @"" : [companyDictionary objectForKey:@"address"];
    NSString *city = [companyDictionary objectForKey:@"city"];
    NSString *state = [companyDictionary objectForKey:@"state"];
    NSString *zipCode = [companyDictionary objectForKey:@"zipcode"];
    NSString *location = [NSString stringWithFormat:@"%@, %@, %@, %@", address, city, state, zipCode];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:location
                 completionHandler:^(NSArray* placemarks, NSError* error){
                     if (placemarks && placemarks.count > 0) {
                         CLPlacemark *topResult = [placemarks objectAtIndex:0];
                         success([[MKPlacemark alloc] initWithPlacemark:topResult]);
                     }
                 }
     ];
}

@end
