//
//  PTRUtils.h
//  PetronautApp
//
//  Created by Luis Fernando Mata on 10/1/16.
//  Copyright © 2016 Petronaut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PTRRequestHelper.h"
#import <MapKit/MapKit.h>

@interface PTRUtils : NSObject

+ (NSString *) stringForFilter: (PTRFilter) filter;
+ (void)searchCompaniesWithFilter: (PTRFilter) searchFilter withString: (NSString *) searchString withAutoComplete: (BOOL) autoComplete withSuccess:(void (^)(NSDictionary *response))success withFailure: (void (^)(NSError *error))failure;
+ (void) placeMarkForCompany: (NSDictionary *) companyDictionary withSuccess:(void (^)(MKPlacemark *placemark))success;

@end
