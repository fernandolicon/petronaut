//
//  PTRSectorsCell.h
//  PetronautApp
//
//  Created by Luis Fernando Mata on 26/8/15.
//  Copyright (c) 2015 Petronaut. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PTRSectorsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *sectorName;
@property (weak, nonatomic) IBOutlet UIImageView *sectorIcon;

@end
