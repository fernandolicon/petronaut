//
//  PTRInformationHeader.h
//  PetronautApp
//
//  Created by Luis Fernando Mata on 17/10/15.
//  Copyright © 2015 Petronaut. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PTRInformationHeader : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *headerTitle;

@end
