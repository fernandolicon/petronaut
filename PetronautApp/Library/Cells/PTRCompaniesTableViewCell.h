//
//  CompaniesTableViewCell.h
//  PetronautApp
//
//  Created by Luis Fernando Mata on 5/12/15.
//  Copyright © 2015 Petronaut. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PTRCompaniesTableViewCell : UITableViewCell

- (void) renderWithCompanyDictionary: (NSDictionary *) company;
- (void) renderForAutocomplete: (NSString *) cellTitle;

@end
