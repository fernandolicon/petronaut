//
//  PTRHeaderView.h
//  PetronautApp
//
//  Created by Luis Fernando Mata on 27/2/16.
//  Copyright © 2016 Petronaut. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MKMapView;

@interface PTRHeaderView : UIView

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *seeFullMapButton;

+ (PTRHeaderView *) instanciateFromNib;

@end
