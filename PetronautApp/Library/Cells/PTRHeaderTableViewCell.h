//
//  PTRHeaderTableViewCell.h
//  PetronautApp
//
//  Created by Luis Fernando Mata on 27/2/16.
//  Copyright © 2016 Petronaut. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PTRHeaderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@end
