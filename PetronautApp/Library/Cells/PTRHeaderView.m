//
//  PTRHeaderView.m
//  PetronautApp
//
//  Created by Luis Fernando Mata on 27/2/16.
//  Copyright © 2016 Petronaut. All rights reserved.
//

#import "PTRHeaderView.h"

@implementation PTRHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+ (PTRHeaderView *)instanciateFromNib {
    return [NSBundle.mainBundle loadNibNamed:@"PTRHeaderView" owner:nil options:nil].firstObject;
}

@end
