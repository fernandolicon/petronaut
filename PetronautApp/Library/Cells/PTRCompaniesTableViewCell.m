//
//  CompaniesTableViewCell.m
//  PetronautApp
//
//  Created by Luis Fernando Mata on 5/12/15.
//  Copyright © 2015 Petronaut. All rights reserved.
//

#import "PTRCompaniesTableViewCell.h"

@implementation PTRCompaniesTableViewCell{
    
    __weak IBOutlet UILabel *companyName;
    __weak IBOutlet UILabel *companyDescription;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) renderWithCompanyDictionary: (NSDictionary *) company{
    companyName.text = [company objectForKey:@"company"];
    NSString *cityState = [NSString stringWithFormat:@"%@, %@", [company objectForKey:@"city"], [company objectForKey:@"state"]];
    companyDescription.text = cityState;
}

- (void) renderForAutocomplete: (NSString *) cellTitle {
    companyName.text = cellTitle;
}

- (void) prepareForReuse{
    [super prepareForReuse];
    
    companyName.text = @"";
    companyDescription.text = @"";
}

@end