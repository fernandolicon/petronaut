//
//  PTRViewHelper.h
//  PetronautApp
//
//  Created by Luis Fernando Mata on 24/11/15.
//  Copyright © 2015 Petronaut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PTRRequestHelper.h"

@interface PTRViewHelper : NSObject

+ (NSArray *) arrayWithResponse: (NSDictionary *) response;
+ (NSDictionary *) companiesAlphabeticallyFromResponse: (NSDictionary *) response;
+ (NSArray *) filterAutoCompleteWithResponse: (NSDictionary *) response andFilter: (PTRFilter) filter forText: (NSString *) searchText;
+ (NSArray *) arrayOfCustomValuesFromArray: (NSArray *) valuesArray;

@end
