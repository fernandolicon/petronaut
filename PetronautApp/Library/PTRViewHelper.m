//
//  PTRViewHelper.m
//  PetronautApp
//
//  Created by Luis Fernando Mata on 24/11/15.
//  Copyright © 2015 Petronaut. All rights reserved.
//

#import "PTRViewHelper.h"
#import "PTRCompany.h"

@implementation PTRViewHelper

+ (NSArray *) arrayWithResponse: (NSDictionary *) response {
    NSMutableArray *companiesArray = [[NSMutableArray alloc] init];
    for (NSDictionary *company in response) {
        [companiesArray addObject:company];
    }
    
    return companiesArray;
}

+ (NSArray *) filterAutoCompleteWithResponse: (NSDictionary *) response andFilter: (PTRFilter) filter forText:(NSString *)searchText{
    NSArray *companies = [PTRViewHelper arrayWithResponse:response];
    
    switch (filter) {
        case PTRAddress:
        case PTRName:
            return [PTRViewHelper arrayOfCompaniesForAutoCompleteFromResponse:response];
        case PTRProducts: {
            NSArray *customValues = [PTRViewHelper arrayOfCustomValuesFromArray:[companies valueForKeyPath:@"products"]];
            customValues = [PTRViewHelper filterArray:customValues ForSearchText:searchText];
            return customValues;
        }
        case PTRServices: {
            NSArray *customValues = [PTRViewHelper arrayOfCustomValuesFromArray:[companies valueForKeyPath:@"services"]];
            customValues = [PTRViewHelper filterArray:customValues ForSearchText:searchText];
            return customValues;
        }
        default:
            return nil;
    }
}

+ (NSArray *) arrayOfCustomValuesFromArray: (NSArray *) valuesArray {
    NSMutableArray *values = [[NSMutableArray alloc] init];
    for (NSString *value in valuesArray) {
        NSArray *stringsArray = [value componentsSeparatedByString:@", "];
        [values addObjectsFromArray:stringsArray];
    }
    
    return  [[NSSet setWithArray:values] allObjects];
}

+ (NSArray *) filterArray: (NSArray *) array ForSearchText: (NSString *) searchString {
    NSMutableArray *someArray = [[NSMutableArray alloc] initWithArray: array];
    NSString *expression=[NSString stringWithFormat:@"SELF contains '%@'",searchString];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:expression];
    [someArray filterUsingPredicate:predicate];
    return someArray;
}

+ (NSDictionary *) companiesAlphabeticallyFromResponse: (NSDictionary *) response {
    NSMutableArray *companiesArray = [[NSMutableArray alloc] init];
    for (NSDictionary *company in response) {
        [companiesArray addObject:company];
    }
    
    return [PTRViewHelper separateDictionaryAlphabetically:companiesArray];
}

+ (NSArray *) arrayOfCompaniesForAutoCompleteFromResponse: (NSDictionary *) response {
    NSMutableArray *topResponse = [[NSMutableArray alloc] init];
    
    for (NSDictionary *company in response) {
        [topResponse addObject:company];
    }
    
    return topResponse;
}

+ (NSDictionary *) separateDictionaryAlphabetically: (NSArray *) companiesArray{
    NSMutableDictionary *orderedDictionary = [NSMutableDictionary dictionary];
    NSMutableArray *nonLettersArray = [[NSMutableArray alloc] initWithArray:companiesArray];
    
    for (char firstChar = 'a'; firstChar <= 'z'; firstChar++)
    {
        NSString *firstCharacter = [NSString stringWithFormat:@"%c", firstChar];
        NSArray *content = [companiesArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K beginswith[cd] %@", @"company", firstCharacter]];
        
        //Remove objects so at then end you have the non alphabetical names
        [nonLettersArray removeObjectsInArray:content];
        
        NSMutableArray *mutableContent = [NSMutableArray arrayWithArray:content];
        
        if ([mutableContent count] > 0)
        {
            NSString *key = [firstCharacter uppercaseString];
            [orderedDictionary setObject:mutableContent forKey:key];
        }
    }
    
    //Add the non alphabetical names
    if (nonLettersArray.count >0){
        [orderedDictionary setObject:nonLettersArray forKey:@"#"];
    }
    
    return orderedDictionary;
}

@end
