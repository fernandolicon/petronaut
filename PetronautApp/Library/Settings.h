//
//  Settings.h
//  PetronautApp
//
//  Created by Luis Fernando Mata on 23/8/15.
//  Copyright (c) 2015 Petronaut. All rights reserved.
//

#ifndef PetronautApp_Settings_h
#define PetronautApp_Settings_h

static NSString * const petroNautAPI = @"https://gopetronaut.com/app/petronautapi/";
static NSString * const amazonCloudSearch = @"http://search-petronaut-2ontaqbm4ex72grvwwzmsdot6a.us-west-2.cloudsearch.amazonaws.com/2013-01-01/search?q=";

#endif
