//
//  MKMapView+PetronautMap.h
//  PetronautApp
//
//  Created by Luis Fernando Mata on 17/10/15.
//  Copyright © 2015 Petronaut. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKMapView (PetronautMap)

- (void) setPlaceMarkForCompany: (NSDictionary *) companyDictionary;

@end
