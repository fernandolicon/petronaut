//
//  PTRRequestHelper.h
//  PetronautApp
//
//  Created by Luis Fernando Mata on 23/8/15.
//  Copyright (c) 2015 Petronaut. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, PTRFilter){
    PTRName,
    PTRAddress,
    PTRProducts,
    PTRServices
};

@interface PTRRequestHelper : NSObject

+ (void)getCompaniesForSector: (NSInteger) sectorID withSuccess:(void (^)(NSDictionary *response))success withFailure: (void (^)(NSError *error))failure;
+ (void)searchCompaniesWithFilter: (PTRFilter) searchFilter withString: (NSString *) searchString withSuccess:(void (^)(NSDictionary *response))success withFailure: (void (^)(NSError *error))failure;
+ (void)performAutoCompleteWithFilter: (PTRFilter) searchFilter withString: (NSString *) searchString withSuccess:(void (^)(NSDictionary *response))success withFailure: (void (^)(NSError *error))failure;

@end
