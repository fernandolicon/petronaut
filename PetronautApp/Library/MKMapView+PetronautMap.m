//
//  MKMapView+PetronautMap.m
//  PetronautApp
//
//  Created by Luis Fernando Mata on 17/10/15.
//  Copyright © 2015 Petronaut. All rights reserved.
//

#import "MKMapView+PetronautMap.h"
@import AddressBook;

@implementation MKMapView (PetronautMap)

- (void) placeMarkForCompany: (NSDictionary *) companyDictionary {
    NSString *address = [[companyDictionary objectForKey:@"address"] isEqualToString:@"PO Box"] ? @"" : [companyDictionary objectForKey:@"address"];
    NSString *city = [companyDictionary objectForKey:@"city"];
    NSString *state = [companyDictionary objectForKey:@"state"];
    NSString *zipCode = [companyDictionary objectForKey:@"zipcode"];
    NSString *location = [NSString stringWithFormat:@"%@, %@, %@, %@", address, city, state, zipCode];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:location
                 completionHandler:^(NSArray* placemarks, NSError* error){
                     if (placemarks && placemarks.count > 0) {
                         CLPlacemark *topResult = [placemarks objectAtIndex:0];
                         MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
                         
                         MKCoordinateRegion region = self.region;
                         region.span.longitudeDelta = 0.01;
                         region.span.latitudeDelta = 0.01;
                         region.center = [(CLCircularRegion *)placemark.region center];
                         
                         [self setRegion:region animated:YES];
                         [self addAnnotation:placemark];
                     }
                 }
     ];
}

- (void) setPlaceMarkForCompany: (NSDictionary *) companyDictionary {
    NSString *latString = [companyDictionary objectForKey:@"lat"];
    NSString *lonString = [companyDictionary objectForKey:@"long"];
    CLLocationCoordinate2D companyCoordinate;
    if ((latString) && (lonString)) {
        companyCoordinate = CLLocationCoordinate2DMake(latString.floatValue, lonString.floatValue);
    }
    
    NSString *address = [[companyDictionary objectForKey:@"address"] isEqualToString:@"PO Box"] ? @"" : [companyDictionary objectForKey:@"address"];
    NSString *city = [companyDictionary objectForKey:@"city"];
    NSString *state = [companyDictionary objectForKey:@"state"];
    NSString *zipCode = [companyDictionary objectForKey:@"zipcode"];
    NSString *location = [NSString stringWithFormat:@"%@, %@, %@, %@", address, city, state, zipCode];

    
    if (companyCoordinate.latitude != 0) {
        NSDictionary *addressDict = @{
                                      (NSString *) kABPersonAddressStreetKey : address,
                                      (NSString *) kABPersonAddressCityKey : city,
                                      (NSString *) kABPersonAddressStateKey : state,
                                      (NSString *) kABPersonAddressZIPKey : zipCode,
                                      };
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:companyCoordinate addressDictionary:addressDict];
        
        MKCoordinateRegion region = self.region;
        region.span.longitudeDelta = 0.05;
        region.span.latitudeDelta = 0.05;
        region.center = placemark.coordinate;
        
        [self setRegion:region animated:YES];
        [self addAnnotation:placemark];
    } else {
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:location
                     completionHandler:^(NSArray* placemarks, NSError* error){
                         if (placemarks && placemarks.count > 0) {
                             CLPlacemark *topResult = [placemarks objectAtIndex:0];
                             MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
                             
                             MKCoordinateRegion region = self.region;
                             region.span.longitudeDelta = 0.01;
                             region.span.latitudeDelta = 0.01;
                             region.center = [(CLCircularRegion *)placemark.region center];
                             
                             [self setRegion:region animated:NO];
                             [self addAnnotation:placemark];
                         }
                     }
         ];
    }
}

- (void) setMapDirectionsForCompany: (NSDictionary *) companyDictionary {
    //MKPlacemark *place = [self setPlaceMarkForCompany:companyDictionary];
}

@end
