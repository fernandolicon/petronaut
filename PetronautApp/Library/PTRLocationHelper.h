//
//  PTRLocationHelper.h
//  PetronautApp
//
//  Created by Luis Fernando Mata on 27/2/16.
//  Copyright © 2016 Petronaut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface PTRLocationHelper : NSObject <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocation *userLocation;
@property (strong, nonatomic) CLLocationManager *locationManager;

+ (id)sharedManager;
- (BOOL) hasAuthorizationFromUser;
- (CLLocation *) userLocation;

@end
