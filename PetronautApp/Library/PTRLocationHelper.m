//
//  PTRLocationHelper.m
//  PetronautApp
//
//  Created by Luis Fernando Mata on 27/2/16.
//  Copyright © 2016 Petronaut. All rights reserved.
//

#import "PTRLocationHelper.h"

@implementation PTRLocationHelper

@synthesize userLocation;
@synthesize locationManager;

#pragma mark Singleton Methods

+ (id)sharedManager {
    static PTRLocationHelper *sharedLocationHelper = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedLocationHelper = [[self alloc] init];
    });
    return sharedLocationHelper;
}

- (id)init {
    if (self = [super init]) {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.distanceFilter = 500;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        if ([locationManager location]) {
            userLocation = [locationManager location];
        }
        [self triggerLocationServices];
    }
    return self;
}

#pragma mark Core Location methods

- (void) triggerLocationServices {
    if (CLLocationManager.locationServicesEnabled) {
        [locationManager requestWhenInUseAuthorization];
    }
}

- (BOOL) hasAuthorizationFromUser {
    return [CLLocationManager locationServicesEnabled];
}

- (CLLocation *) userLocation {
    return userLocation;
}

@end