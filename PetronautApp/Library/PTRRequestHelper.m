//
//  PTRRequestHelper.m
//  PetronautApp
//
//  Created by Luis Fernando Mata on 23/8/15.
//  Copyright (c) 2015 Petronaut. All rights reserved.
//

#import "PTRRequestHelper.h"
#import "AFHTTPRequestOperationManager.h"
#import "Settings.h"
#import "PTRUtils.h"

@implementation PTRRequestHelper

+ (void)getCompaniesForSector: (NSInteger) sectorID withSuccess:(void (^)(NSDictionary *response))success withFailure: (void (^)(NSError *error))failure{
    NSString *url = [NSString stringWithFormat:@"%@getCompanies.php?selection=%ld", petroNautAPI, (long) sectorID];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *strUrl=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    
    [manager GET:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

+ (void)searchCompaniesWithFilter: (PTRFilter) searchFilter withString: (NSString *) searchString withSuccess:(void (^)(NSDictionary *response))success withFailure: (void (^)(NSError *error))failure{
    NSString *url = [NSString stringWithFormat:@"%@searchCompanies.php?filter=%ld&search=%@", petroNautAPI, (long) searchFilter, searchString];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *strUrl=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    
    [manager GET:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

+ (void)performAutoCompleteWithFilter: (PTRFilter) searchFilter withString: (NSString *) searchString withSuccess:(void (^)(NSDictionary *response))success withFailure: (void (^)(NSError *error))failure {
    NSString *url = [NSString stringWithFormat:@"%@searchAutocomplete.php?filter=%ld&search=%@", petroNautAPI, (long) searchFilter, searchString];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *strUrl=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    
    [manager GET:strUrl parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}
/*
+ (void)performAutoCompleteWithFilter: (PTRFilter) searchFilter withString: (NSString *) searchString withSuccess:(void (^)(NSDictionary *response))success withFailure: (void (^)(NSError *error))failure{
    //(and+(prefix+field%3D'company'+'petro'))&q.parser=structured
    NSString *url = [NSString stringWithFormat:@"%@(and+(prefix+field%%3D'%@'+'%@'))&q.parser=structured", amazonCloudSearch, [PTRUtils stringForFilter:searchFilter], searchString];
    NSLog(@"URL: %@", url);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        NSDictionary *responses = [responseObject objectForKey:@"hits"];
        NSDictionary *hits = [responses objectForKey:@"hit"];
        success(hits);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@",error);
        failure(error);
    }];
}*/

//searchCompanies.php?filter=0&search=Field

@end
