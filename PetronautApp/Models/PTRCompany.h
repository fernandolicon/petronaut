//
//  PTRCompany.h
//  PetronautApp
//
//  Created by Luis Fernando Mata on 16/1/16.
//  Copyright © 2016 Petronaut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface PTRCompany : NSObject

@property NSInteger _id;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *company;
@property (nonatomic, strong) NSArray *officeNumbers;
@property (nonatomic, strong) NSString *sector;
@property (nonatomic, strong) NSArray *services;
@property (nonatomic, strong) NSArray *products;
@property (nonatomic, strong) NSString *state;
@property NSInteger zipCode;
@property (nonatomic, strong) NSURL *website;
@property CLLocationCoordinate2D companyCoordinate;
@property BOOL featured;
@property (nonatomic, strong) NSString *mission;

//- (instancetype) initWithDictionary: (NSDictionary *) companyDictionary;
- (instancetype) initWithDictionaryForAutoComplete: (NSDictionary *) companyDictionary;

@end
