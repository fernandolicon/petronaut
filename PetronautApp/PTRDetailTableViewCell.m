//
//  PTRDetailTableViewCell.m
//  PetronautApp
//
//  Created by Luis Fernando Mata on 27/2/16.
//  Copyright © 2016 Petronaut. All rights reserved.
//

#import "PTRDetailTableViewCell.h"
#import <AMTagListView/AMTagListView.h>

@implementation PTRDetailTableViewCell {
    __weak IBOutlet UILabel *headerLabel;
    __weak IBOutlet UILabel *dataLabel;
    __weak IBOutlet NSLayoutConstraint *viewHeightConstraint;
    NSString *type;
}

@synthesize tagsView;

- (void)awakeFromNib {
    // Initialization code
    [[AMTagView appearance] setTagLength:10];
    [[AMTagView appearance] setTextPadding:CGPointMake(14, 14)];
    [[AMTagView appearance] setBackgroundColor:[UIColor clearColor]];
    [[AMTagView appearance] setRadius:0.0];
    [[AMTagView appearance] setTagLength:0.0];
    [[AMTagView appearance] setTagColor:[UIColor colorWithRed:42/255.f green:107/255.f blue:196/255.f alpha:1.0]];
}

- (void) renderCellWithDictionary: (NSDictionary *) dataDictionary{
    [headerLabel setText:[dataDictionary objectForKey:@"header"]];
    [dataLabel setText:[dataDictionary objectForKey:@"data"]];
}

- (void) renderWithTagsForDictionary: (NSDictionary *) dataDictionary {
    dataLabel.hidden = YES;
    tagsView.hidden = NO;
    [headerLabel setText:[dataDictionary objectForKey:@"header"]];
    NSArray *dataValues = [PTRDetailTableViewCell arrayOfDataFromString:[dataDictionary objectForKey:@"data"]];
    
    for (NSString *value in dataValues) {
        [tagsView addTag:value];
    }
    
    [tagsView setTapHandler:^(AMTagView *view) {
        [self.delegate didSelectTagWithString:view.tagText forHeader:headerLabel.text];
    }];

    viewHeightConstraint.constant = tagsView.contentSize.height;
}

+ (CGFloat)heightForCellWithWithData:(NSDictionary *) data {
    return 100;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    viewHeightConstraint.constant = 21;
    self.delegate = nil;
    [tagsView removeAllTags];
    dataLabel.hidden = NO;
    tagsView.hidden = YES;
    headerLabel.text = @"";
    dataLabel.text = @"";
}

+ (NSArray *) arrayOfDataFromString: (NSString *) dataString {
    return [dataString componentsSeparatedByString:@", "];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (NSString *)cellHeader {
    return headerLabel.text;
}

@end
