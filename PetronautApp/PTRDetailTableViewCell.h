//
//  PTRDetailTableViewCell.h
//  PetronautApp
//
//  Created by Luis Fernando Mata on 27/2/16.
//  Copyright © 2016 Petronaut. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AMTagListView;

@protocol PTRDetailTableViewCellDelegate <NSObject>

- (void) didSelectTagWithString: (NSString *) selectedString forHeader: (NSString *) header;

@end

@interface PTRDetailTableViewCell : UITableViewCell

@property CGFloat contentSize;
@property (weak, nonatomic) IBOutlet AMTagListView *tagsView;
@property (nonatomic, assign) id<PTRDetailTableViewCellDelegate> delegate;

- (void) renderCellWithDictionary: (NSDictionary *) dataDictionary;
- (void) renderWithTagsForDictionary: (NSDictionary *) dataDictionary;
+ (CGFloat)heightForCellWithWithData:(NSDictionary *) data;
- (NSString *) cellHeader;

@end
