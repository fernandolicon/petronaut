//
//  AppDelegate.h
//  PetronautApp
//
//  Created by Luis Fernando Mata on 11/8/15.
//  Copyright (c) 2015 Petronaut. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PTRAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

