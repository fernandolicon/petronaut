//
//  main.m
//  PetronautApp
//
//  Created by Luis Fernando Mata on 11/8/15.
//  Copyright (c) 2015 Petronaut. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PTRAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PTRAppDelegate class]));
    }
}
